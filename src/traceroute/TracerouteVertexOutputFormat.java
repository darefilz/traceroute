package traceroute;

import java.io.IOException;
import org.apache.giraph.graph.Vertex;
import org.apache.giraph.io.formats.TextVertexOutputFormat;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

/**
 * @author Fabian
 */
public class TracerouteVertexOutputFormat extends TextVertexOutputFormat<Text, IntWritable, IntWritable> {

	@Override
	public TextVertexWriter createVertexWriter(TaskAttemptContext tac) throws IOException, InterruptedException {
		return new TextVertexWriterToEachLine() {
			@Override
			protected Text convertVertexToLine(Vertex<Text, IntWritable, IntWritable, ?> vertex) throws IOException {
				//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
				return new Text(vertex.getId() + "\t" + vertex.getValue());
			}
		};
	}
}
