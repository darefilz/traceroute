package traceroute;

import java.io.IOException;
import org.apache.giraph.graph.Vertex;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;

/**
 * @author Fabian
 */
public class TracerouteVertex extends Vertex<Text, IntWritable, IntWritable, NullWritable> {

	@Override
	public void compute(Iterable<NullWritable> itrbl) throws IOException {
		System.out.println("TODO: compute");
	}
}
