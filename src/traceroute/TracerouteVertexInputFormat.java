package traceroute;

import java.io.IOException;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.giraph.edge.EdgeFactory;
import org.apache.giraph.graph.Vertex;
import org.apache.giraph.io.formats.TextVertexInputFormat;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

/**
 * @author Fabian
 */
public class TracerouteVertexInputFormat extends TextVertexInputFormat<Text, IntWritable, IntWritable> {

	private static class Hop {

		public String IpAddress;
		public int RoundTripTime;

		public Hop(String ipAddress, int roundTripTime) {
			this.IpAddress = ipAddress;
			this.RoundTripTime = roundTripTime;
		}
	}

	private final static IntWritable ONE = new IntWritable(1);
	private final static Pattern IP_PATTERN = Pattern.compile("\\d+:(\\d{1,3}(?:\\.\\d{1,3}){3}):(.*?);");
	private final static Pattern RTT_PATTERN = Pattern.compile("(\\d+)ms"); //"(\\d+(?:\\.\\d+)?) ?ms" for float values
	private LinkedList<Hop> vertices = new LinkedList<>();

	@Override
	public TextVertexReader createVertexReader(InputSplit split, TaskAttemptContext context) throws IOException {
		return new TextVertexReader() {
			@Override
			public boolean nextVertex() throws IOException, InterruptedException {
				return vertices.size() >= 2 || getRecordReader().nextKeyValue();
			}

			@Override
			public Vertex<Text, IntWritable, IntWritable, ?> getCurrentVertex() throws IOException, InterruptedException {
				if(vertices.size() < 2) {
					vertices.clear();
					parseTraceroute(getRecordReader().getCurrentValue().toString());
				}
				Hop sourceHop = vertices.poll();
				Hop targetHop = vertices.peek();
				TracerouteVertex vertex = new TracerouteVertex();

				vertex.initialize(new Text(sourceHop.IpAddress), ONE);
				vertex.addEdge(EdgeFactory.create(new Text(targetHop.IpAddress), new IntWritable(targetHop.RoundTripTime)));
				return vertex;
			}

			//TODO: kann aus jeder Zeile mindestens ein Vertex erstellt werden?
			private void parseTraceroute(String line) {
				Matcher ipMatcher = IP_PATTERN.matcher(line);

				while(ipMatcher.find()) {
					Matcher rttMatcher = RTT_PATTERN.matcher(ipMatcher.group(2));
					int rttSum = 0;
					int probeCount = 0;

					while(rttMatcher.find()) {
						rttSum += Integer.parseInt(rttMatcher.group(1));
						probeCount++;
					}
					vertices.add(new Hop(ipMatcher.group(1), rttSum / probeCount));
				}
			}
		};
	}
}